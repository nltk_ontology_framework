from BeautifulSoup import BeautifulSoup
import os
import re

def get_terms(file):
    soup = BeautifulSoup(open(file).read())
    body = soup.find('div', attrs={"class": "KonaBody"})
    body.find('span', attrs={"class": "rightbox"}).decompose()
    p = body.find('p')
    for a in p.findAll('a'): a.decompose()
    for line in str(p).split('<br />'):
      line = re.sub('&nbsp;.*$', '', line, re.S | re.M)
      line = re.sub('\s+', ' ', line, re.S | re.M)
      line = re.sub('<[^>]+>', '', line).lstrip().rstrip().lower()
      #line = re.sub('\s
      print line
    #for i in body.findAll('i'):
    #   print '"%s"' % (i.text)
    return []


terms = []
for file in os.listdir('.'):
    if file.endswith(".html"):
       terms.extend(get_terms(file))
print terms
