# This Python file uses the following encoding: utf-8
'''
Created on May 3, 2011

@author: mjacob
'''
import os
import re
import yaml
import nltk
from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktWordTokenizer
from nltk.chunk.regexp import RegexpParser
from nltk.corpus import wordnet as wn
from itertools import chain

XML_ILLEGAL = re.compile(u'([\u0000-\u0008\u000b-\u000c\u000e-\u001f\ufffe-\uffff])' + \
                 u'|' + \
                 u'([%s-%s][^%s-%s])|([^%s-%s][%s-%s])|([%s-%s]$)|(^[%s-%s])' % \
                  (unichr(0xd800),unichr(0xdbff),unichr(0xdc00),unichr(0xdfff),
                   unichr(0xd800),unichr(0xdbff),unichr(0xdc00),unichr(0xdfff),
                   unichr(0xd800),unichr(0xdbff),unichr(0xdc00),unichr(0xdfff)))

KILL_END_PUNCT = re.compile('^(.*\w)\W*$')
HIGHLIGHTS = re.compile('⟦([^⟧]*)⟧')
PUNCT = re.compile('[\W\']')
SPACE = re.compile('^\s*$')

chunker_grammar = """NP: {<JJ|CD|N.*>+}"""

corpus_dir = '../symbol_corpus'

files = [(open(os.path.join(corpus_dir, x)).read(), x) for x in os.listdir(corpus_dir) if x.endswith('.txt')]

longest_file = max(files, key=lambda x: len(x[0]))[0]
print len(files)
print len(longest_file)

sentence_tokenizer = PunktSentenceTokenizer(longest_file)
word_tokenizer = PunktWordTokenizer()
chunker = RegexpParser(chunker_grammar)

def split_term(term):
    tokens = filter(lambda x: not SPACE.match(x), term.split(' '))
    for i in range(len(tokens)):
        for j in range(i+1, len(tokens)):
            yield " ".join(tokens[i:j])

def unhighlight(sentence):
    return (sentence.replace('⟦', '').replace('⟧', ''), HIGHLIGHTS.findall(sentence))

statistics = {}
for file, filename in files:
    tokencounts = {}
    for sentence in sentence_tokenizer.tokenize(file):
        unhighlighted, highlights = unhighlight(sentence)
        unpunctuated = KILL_END_PUNCT.match(unhighlighted)
        if unpunctuated:
            unhighlighted = unpunctuated.group(1)
        else:
            print "empty sentence in %s: %s" % (filename, sentence)
            continue
        
        tokens = list(XML_ILLEGAL.sub('', word) for word in word_tokenizer.tokenize(unhighlighted))
        tagged = nltk.pos_tag(tokens)
        chunked = chunker.parse(tagged)
        np_chunks = [" ".join(y[0] for y in x.leaves()) for x in chunked.subtrees(lambda x: x.node == 'NP')]
        for chunk in chain(np_chunks, highlights):
            term = PUNCT.sub(' ', chunk).lstrip().rstrip().lower()
            if not term:
                continue
            
            for subterm in split_term(term):
                if subterm in tokencounts:
                    tokencounts[subterm] += 1
                else:
                    tokencounts[subterm] = 1
        
        #for chunk in list(tokencounts.keys()):
        #    if wn.synsets(chunk):
        #        del tokencounts[chunk]
                
    for term, count in tokencounts.items():
        if term in statistics:
            statistics[term].append(count)
        else:
            statistics[term] = [count]

""" remove singletons """
for term, count in statistics.items():
    if len(count) == 1 and count[0] == 1:
        del statistics[term]

yaml.dump(statistics, open(os.path.join(corpus_dir, 'statistics'), 'w'))
