# -*- coding: utf-8 -*-
from BeautifulSoup import BeautifulSoup, BeautifulStoneSoup

import os
import re

for root, dirs, files in os.walk('www.symbols.com/encyclopedia'):
    for file in files:
        if re.match('.*\.html$', file) and file != "index.html":
            text = open(os.path.join(root, file)).read()
            p = BeautifulSoup(text)
            div = p.find('div', attrs={"class": "KonaBody"})
            span = p.find('span')
            span.decompose()
            b = div.find('b')
            category = b.text.split(' ')[0]
            b.decompose()

            #print '-'*79
            #print category

            for img in div.findAll('a'):
                href = img['href']
                m = re.search('\/(\d[\d\w]*)\.\w+$', href)
                if m:
                    hulb = "#"+m.group(1)
                else:
                    hulb = img.text
                img.replaceWith(u' ⟦%s⟧ '% (hulb))
            for img in div.findAll('img'):
                href = img['src']
                m = re.search('\/([\d\w]+)\.\w+$', href)
                if m:
                    hulb = "img#"+m.group(1)
                else:
                    hulb = img.text
                if hulb:
                    img.replaceWith(u' ⟦%s⟧ '% (hulb.rstrip().lstrip()))
                else:
                    img.replaceWith('')
            for t in div.findAll('p'):
                for s in t.findAll():
                    if s.text and s.text != '&nbsp;&nbsp;&nbsp;&nbsp;':
                        s.replaceWith(u' ⟦%s⟧ ' % (s.text.rstrip().lstrip()))
                    else:
                        s.replaceWith('')

                
            x = str(BeautifulStoneSoup(str(div), convertEntities=BeautifulStoneSoup.HTML_ENTITIES))

            x = re.sub('<[^\>]+?>', '', x)
            x = re.sub('[ \n\t\r]+', ' ', x)
            with open(category.replace(':', '_'), 'w') as file:
                file.write(x.rstrip().lstrip())
            #print div

