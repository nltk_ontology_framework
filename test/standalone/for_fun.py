# This Python file uses the following encoding: utf-8
'''
Created on May 8, 2011

@author: mjacob
'''

from mjacob.ontologybuilder import SimpleConcepts
from nltk.corpus import wordnet
wn = wordnet

a = SimpleConcepts()
def gogo(thing):
    print " ".join([wn.synset(n).definition 
                    for n in  a.get_concept([[s.name for s in wn.synsets(x, pos='an')] 
                                             for x in thing.split(' ')], debug=True)])
    

gogo('hobo sign')