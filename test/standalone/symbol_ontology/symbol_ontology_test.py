# This Python file uses the following encoding: utf-8
'''
Created on May 7, 2011

@author: mjacob

This is a sample usage of the ontology framework developed in this project,
which generates a concept tree from terms discovered in a corpus of texts
which describe various symbols, pulled from http://www.symbols.com/

As that site is under copyright, the corpus I've been using to run this test
is not distributed with this source code. 
'''
import os
import yaml
from glob import glob
import nltk
from nltk.corpus.reader.plaintext import PlaintextCorpusReader
from nltk.tokenize.regexp import RegexpTokenizer
from nltk.chunk.regexp import RegexpParser
from mjacob.ontologybuilder import (OntologyBuilderFramework
                                  , HighlightedTerms
                                  , SimpleWordnetSynonyms
                                  , SimpleConcepts
                                  , SimpleConceptHierarchies
                                  , SimpleChunkFilter
                                  , SimpleSubterms
                                  , NavigliTermFilter
                                    )
from util.cached import Cacheable

VERSION = 4
NAME = 'symbol ontology generator V.%s' % (VERSION)
BASEDIR = os.path.split(os.path.abspath(__file__))[0] 
CACHEDIR = os.path.join(BASEDIR, 'symbol_corpus_cache')
CORPUSDIR = os.path.join(BASEDIR, 'symbol_corpus')
CORPUSFILES = os.path.join(CORPUSDIR, '*.txt')

class SymbolOntologyBuilder(OntologyBuilderFramework, 
                            HighlightedTerms, 
                            SimpleWordnetSynonyms, 
                            SimpleConcepts, 
                            SimpleConceptHierarchies,
                            Cacheable,
                            dict):
    """
    SymbolOntologyBuilder is implemented on top of the C{OntologyBuilderFramework}, as 
    a combination of C{HighlightedTerms}, C{SimpleWordnetSynonyms}, C{SimpleConcepts},
    and C{SimpleConceptHierarchies}, as well as being C{Cacheable}.
    
    Note that all of the components REQUIRE that the base object be C{Cacheable}
    """
    
    OTHER_CORPORA = "terms.other_corpora"
    ALPHA = "terms.NavigliTermFilter.alpha"
    THRESHOLD = "terms.NavigliTermFilter.threshold"
    
    def __init__(self, parameter_file, only_do=None, ignore_cache=None):
        """
        @param paramter_file: a file containing parameters for the SymbolOntologyGenerator 
        @param only_do: if specified, only perform steps in this collection
        @param ignore_cache: if specified, ignore any cached results from 
            steps specified in this collection. note that any new results 
            will still be saved to cache, possibly overwriting existing results.
        """
        self.__set_parameters(parameter_file)
        self.__init_cacheable(ignore_cache=ignore_cache)
        self.__init_framework(only_do=only_do)
        self.__init_terms()
        self.__init_synonyms()
        self.__init_concepts()
        self.__init_concept_hierarchies()
    
    def __init_cacheable(self, ignore_cache):
        """
        Initialize the object as cacheable
        """
        Cacheable.__init__(self, 
                           os.path.join(CACHEDIR, NAME), 
                           ignore_cache=ignore_cache,
                           debug=True)
    
    def __set_parameters(self, parmeter_file):
        """
        set parameters from the @param parameter file
        """
        with open(parmeter_file) as file:
            dict.__init__(self, yaml.load(file.read()))
            
    def __init_framework(self, only_do=None):
        """
        initialize the framework
        """
        OntologyBuilderFramework.__init__(self, 
                                          NAME, 
                                          only_do=only_do)
    
    def __init_terms(self):
        """
        Initialize the term extraction process.
        
        This creates a regular and highlight corpus, identifies the other coprora
        specified in the parameters file, and creates basic tagger, chunker, 
        chunk_filter, subterm, and term filter objects.
        
        For the regular corpus, the words are found using the regexp 
            'img#\w+|#\d+|(\w\.){2:}|[\w\']+'
        which identifies image references, numbers, abbreviations, 
        apostrophes, and word characters.
        
        For the highlight corpus, terms are found using the regexp
            '(?<=<)[^>]+(?=>)'
        which is basically anything between angle backets.
        
        The tagger is a fairly sophisticated model that is part of C{nltk}
        
        The chunker assumes noun phrases consist of adjectives, nouns, and numbers.
        
        The term filter is a  C{NavigliTermFilter} which is parameterized by the values
        in the parameter file.
        
        """
        corpus = PlaintextCorpusReader(root=CORPUSDIR,
                                       fileids=[os.path.split(file)[1] for file in glob(CORPUSFILES)],
                                       word_tokenizer=RegexpTokenizer(r'img#\w+|#\d+|(\w\.){2:}|[\w\']+'),
                                       encoding='utf-8')
        highlights = PlaintextCorpusReader(root=CORPUSDIR,
                                           fileids=[os.path.split(file)[1] for file in glob(CORPUSFILES)],
                                           word_tokenizer=RegexpTokenizer(r'(?<=<)[^>]+(?=>)'),
                                           encoding='utf-8')
        other_corpora = self[SymbolOntologyBuilder.OTHER_CORPORA]
        tagger = nltk.data.load('taggers/maxent_treebank_pos_tagger/english.pickle')
        chunker = RegexpParser('NP: {<JJ|CD|N.*>+}')
        chunk_filter = SimpleChunkFilter('NP', minlength=2)
        subterms = SimpleSubterms(minlength=2)
        term_filter = NavigliTermFilter(self[SymbolOntologyBuilder.ALPHA], 
                                        self[SymbolOntologyBuilder.THRESHOLD],
                                        debug=True)
        
        HighlightedTerms.__init__(self, 
                                  corpus, 
                                  highlights, 
                                  other_corpora, 
                                  tagger, 
                                  chunker, 
                                  chunk_filter, 
                                  subterms, 
                                  term_filter,
                                  debug=True)
    
    def __init_synonyms(self):
        SimpleWordnetSynonyms.__init__(self)
    
    def __init_concepts(self):
        SimpleConcepts.__init__(self)
    
    def __init_concept_hierarchies(self):
        SimpleConceptHierarchies.__init__(self)
        
if __name__ == "__main__":
    builder = SymbolOntologyBuilder('symbol_ontology_builder.yaml',
                                    only_do=set((
                                                 OntologyBuilderFramework.TERMS,
                                                 OntologyBuilderFramework.SYNONYMS,
                                                 OntologyBuilderFramework.CONCEPTS,
                                                 OntologyBuilderFramework.CONCEPT_HIERARCHIES
                                                 )), 
                                    ignore_cache=set((
                                                      #OntologyBuilderFramework.TERMS,
                                                      #OntologyBuilderFramework.SYNONYMS,
                                                      #OntologyBuilderFramework.CONCEPTS,
                                                      #OntologyBuilderFramework.CONCEPT_HIERARCHIES,
                                                      )))
    state = builder.process()
    #print "\n".join(sorted(state["terms"]))
    if OntologyBuilderFramework.CONCEPT_HIERARCHIES in state:
        tree = state[OntologyBuilderFramework.CONCEPT_HIERARCHIES]
        print "%s leaves" % (len(tree.leaves()))
        print "%s internal nodes" % (len(list(tree.subtrees())))
        print "%s depth" % (max(len(x) for x in tree.treepositions()))
        max_concept_tree = max([subtree 
                                for subtree in tree.subtrees() 
                                if (filter(lambda x: type(x) is nltk.tree.Tree, subtree)
                                    and 
                                    reduce(bool.__and__, [len(subsubtree.node) > 1 
                                                          for subsubtree in subtree.subtrees() 
                                                          if subsubtree is not subtree]))],
                               key=lambda x: len(list(x.subtrees())))
        print "%s node" % (max_concept_tree.node)
        print "%s leaves" % (len(max_concept_tree.leaves()))
        print "%s internal nodes" % (len(list(max_concept_tree.subtrees())))
        print "%s depth" % (max(len(x) for x in max_concept_tree.treepositions()))
    
    
    # this will display the concept hierarchies, but takes A LONG TIME
    #tree.draw()
    import nltk
    nltk.tree.Tree.draw
