# This Python file uses the following encoding: utf-8
'''
Created on May 12, 2011

@author: mjacob
'''

import functools

class Memoized(object):
    def __init__(self, f):
        self.__f = f
        self.__cache = {}
    
    def __call__(self, *args):
        key = args[1:]
        if key in self.__cache:
            return self.__cache[key]
        else:
            result = self.__f(*args)
            self.__cache[key] = result
            return result

    def __repr__(self):
        """Return the function's docstring."""
        return self.func.__doc__

    def __get__(self, obj, objtype):
        """Support instance methods."""
        return functools.partial(self.__call__, obj)