# This Python file uses the following encoding: utf-8
'''
Created on May 11, 2011

@author: mjacob
'''
import os
import functools
import cPickle

class Cacheable(object):
    """interface for a class to have results which are cacheable on disk."""
    
    def __init__(self, cachedir, ignore_cache=None, debug=False):
        """
        @param cachedir: where to store the cached files on disk. if it does not exist, it will be created.
        @param ignore_cache: a collection of components which should not be cached by this instance.
        """
        self.__cachedir = cachedir
        self.__ignore_cache = ignore_cache
        self.__debug = debug
        
    def _filename(self, component):
        """helper method to generate the filename for a component"""
        return os.path.join(self.__cachedir, component)
    
    def is_cached(self, component):
        """
        returns C{True} if the specified C{component} is cached
    
        @type component: C{str} 
        @rtype: C{bool}
        """
        if self.__ignore_cache and component in self.__ignore_cache:
            return False
        
        filename = self._filename(component)
        found = os.path.exists(filename)
        
        if self.__debug:
            if found:
                print "cached file found: %s" % (filename)
            else: 
                print "cached file not found: %s" % (filename)
        
        return found

    def get_cache(self, component, loader=cPickle):
        """
        returns the specified cached component
        
        @type component: C{str} 
        @rtype: object
        """
        filename = self._filename(component)
        
        if self.__debug:
            print "reading '%s' from cache" % (filename)
        
        with open(filename) as file:
            return loader.load(file)

        if self.__debug:
            print "finished reading '%s' from cache" % (filename)
        

    def set_cache(self, component, object, dumper=cPickle):
        """
        writes the specified component to cache. 
    
        @type component: C{str} 
        @type object: anything pickle-able 
        """
        
        if not os.path.exists(self.__cachedir):
            os.makedirs(self.__cachedir)
        
        filename = self._filename(component)

        if self.__debug:
            print "writing '%s' to cache" % (filename)
        
        with open(filename, 'w') as file:
            dumper.dump(object, file)

        if self.__debug:
            print "finished writing '%s' to cache" % (filename)
        
        return filename


class Cached(object):
    """
    annotation to indicate a cached function.
    
    using it requires specifying a "key" function with which to generate a filename
    to use as the cache for the result of a specific function call.
    """
    
    def __init__(self, key=lambda *x, **y: str(x) + str(y), loader=cPickle, dumper=cPickle):
        self.__key = key
        self.__loader = loader
        self.__dumper = dumper
        
    def __call__(self, func):
        def caller(*args, **kwargs):
            cacher = args[0]
            if kwargs:
                component = self.__key(*args[1:], **kwargs)
            else:
                component = self.__key(*args[1:])
            if cacher.is_cached(component):
                return cacher.get_cache(component, self.__loader)
            else:
                if kwargs:
                    result = func(*args, **kwargs)
                else:
                    result = func(*args)
                cacher.set_cache(component, result, self.__dumper)
                return result
        return caller
    
    def __repr__(self):
        """Return the function's docstring."""
        return self.func.__doc__
    
    def __get__(self, obj, objtype):
        """Support instance methods."""
        return functools.partial(self.__call__, obj)
