# This Python file uses the following encoding: utf-8
'''
Created on May 3, 2011

@author: mjacob
'''

from framework import OntologyBuilderTerms

import math
from util.cached import Cached
from mjacob.ontologybuilder.framework import OntologyBuilderFramework

class HighlightedTerms(OntologyBuilderTerms):
    """
    this is an term extractor for use in generating a domain.
    
    given a corpus in which terms may or may not be highlighted, term candidates
    are extracted using a tagger and a chunker. term candidates are chosen
    for use if they fit certain statistical patterns in relation to term candidates in
    other corpora.  
    """
    
    def __init__(self, 
                 corpus, 
                 highlights, 
                 other_corpora, 
                 tagger, 
                 chunker, 
                 chunk_filter, 
                 subterms, 
                 statistical_filter,
                 debug=False):
        """
        @type corpus: C{nltk.corpus.reader.api.CorpusReader}
        @param corpus: a body of files tokenized by sentence and word
        @type corpus: C{nltk.corpus.reader.api.CorpusReader}
        @param highlights: the same body of files, but tokenized over highlighted
            portions of the text. 
        @type other_corpora: C{list} of C{nltk.corpus.reader.api.CorpusReader} 
        @type tagger: C{nltk.tag.api.TaggerI}
        @param tagger: a tagger to apply to the corpora
        @type chunker: C{nltk.chunk.api.ChunkParserI}
        @param chunker: a chunker used to mark terms in a tagged sentence
        @type chunk_filter: function(C{nltk.tree.Tree}) -> C{list} of C{list}s
        @param chunk_filter: a function which will extract the relevant components of a chunked sentence
        @type subterms: function(list) -> C{list} of C{str}
        @param subterms: a function which, given tokens, returns potential subterm strings of a term
        @type statistical_filter: function(C{dict}, C{dict}, C{dict}) -> C{set}
        @param statistical_filter: a method for extracting statistically relevant terms
            @param 1: a set of terms 
            @param 2: a dict relating terms to their relative occurrence in the target corpus vs. the other corpora
            @param 3: a dict relating terms to their entropic relevance in the domain corpus 
            
        """
        self.__corpus = corpus
        self.__highlights = highlights
        self.__other_corpora = other_corpora
        self.__tagger = tagger
        self.__chunker = chunker
        self.__chunk_filter = chunk_filter
        self.__subterms = subterms
        self.__statistical_filter = statistical_filter
        self.__debug = debug
    
    @Cached(lambda *x, **y: OntologyBuilderFramework.TERMS)
    def _get_terms(self, **state):
        """input: 1 domain corpus, 1 general corpus
        identify term candidates in each (NPs that fit some statistics)
        remove terms identified in general corpus from domain corpus"""
        
        main_statistics = self.__get_statistics(self.__corpus, "main", self.__highlights)
        
        term_relevences = self.__get_term_relevences(main_statistics)
        term_entropies = self.__get_term_entropies(main_statistics, len(self.__corpus.fileids()))
        
        terms = self.__statistical_filter(main_statistics, term_relevences, term_entropies)
        
        return terms
        
    @Cached(lambda *x: "entropies")
    def __get_term_entropies(self, statistics, doc_count):
        """
        calculate the normalized entropy of a term,
        N = total number of documents
        n = the number of documents the term appears in
        p = n/N 
        normalized entropy H = p * log p / log(N)
        http://www.xycoon.com/normalized_entropy.htm
        """

        entropies = {}
        for term in statistics:
            prob = (1.0*len(statistics[term])) / doc_count
            entropies[term] = prob * math.log(1/prob, 2)
        for term in entropies:
            entropies[term] /= math.log(doc_count) *.003 # scale it up to make it easier to calculate w/
        
        return entropies
    
    def __get_term_counts(self, statistics):
        """
        compute the total number of times each term occurs in the given statistics
        """
        term_counts = {}
        
        for term in statistics:
            term_counts[term] = sum(statistics[term].values())
        
        return term_counts
        
    @Cached(lambda *x: "relevences")
    def __get_term_relevences(self, main_statistics):
        """
        computer the "relevance" each term has in the domain corpus
        
        relevance[term] = n / M, 
        
        where n is the number of times the term occurs in the domain corpus,
        and m is the number of times it occurs in all corpuses
        """
        other_stats = [self.__get_statistics(other_corpus, other_corpus_name, None)
                       for other_corpus, other_corpus_name in self.__other_corpora]
        
        term_relevences = {}
        
        main_term_counts = self.__get_term_counts(main_statistics)
        other_term_counts = [self.__get_term_counts(stat) for stat in other_stats]
        
        for term in main_statistics:
            numerator = main_term_counts[term]
            denominator = sum(other_term_count.get(term,0) for other_term_count in other_term_counts)
            term_relevences[term] = (1.0*numerator) / (numerator + denominator)
        
        return term_relevences

    @Cached(lambda corpus, corpus_name, highlights: "%s.statistics" % (corpus_name))
    def __get_statistics(self, corpus, corpus_name, highlights):
        """
        discovers how many times each term appears in the specified corpus
        """
        if self.__debug:
            print "finding terms in %s" % (corpus_name)

        term_statistics = {}
        for file in corpus.fileids():
            if self.__debug:
                print "processing %s" % (file)
            termcounts_in_file = {}
            sentences = corpus.sents(file)
            
            for sentence in sentences:
                """discover term candidates and count them"""
                tagged = self.__tagger.tag(sentence)
                chunked = self.__chunker.parse(tagged)
                relevent_chunks = self.__chunk_filter(chunked)
                for chunk in relevent_chunks:
                    for subterm in self.__subterms([x[0] for x in chunk]):
                        if subterm in termcounts_in_file:
                            termcounts_in_file[subterm] += 1
                        else:
                            termcounts_in_file[subterm] = 1
            
            if highlights:
                """process the highlights, ammending to existing terms"""
                sentence_highlights = highlights.sents(file)
                for term_sentence in sentence_highlights:
                    for term in term_sentence:
                        for subterm in self.__subterms(term.split(' ')):
                            if subterm in termcounts_in_file:
                                termcounts_in_file[subterm] += 1
                            else:
                                termcounts_in_file[subterm] = 1
            
            """ now, accumulate all the statistics """
            for term, count in termcounts_in_file.items():
                if term in term_statistics:
                    term_statistics[term][file] = count
                else:
                    term_statistics[term] = {file: count}
            if self.__debug:
                print "finished processing %s" % (file)
                    
        return term_statistics

                
