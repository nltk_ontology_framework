# This Python file uses the following encoding: utf-8
'''
Created on May 5, 2011

@author: mjacob
'''
from framework import OntologyBuilderFramework, OntologyBuilderSynonyms
import nltk
from util.cached import Cached
wn = nltk.corpus.wordnet
wn.synsets # this is to get rid of stupid errors in my IDE due to nltk's lazy loading stuff
from itertools import chain
import re

POSSESIVE = re.compile(r"(?:'s|')")
DETERMINERS = set(('the', 'The', 'a', 'as', 'A', 'that', 'That', 'this', 'This'))
PREPOSITIONS = set(('to', 'To', 'in', 'In', 'on', 'On'))

EMPTY_SYNSET = frozenset()

class SimpleWordnetSynonyms(OntologyBuilderSynonyms):
    """
    Constructs synonyms from terms.
    Two terms are deemed synonyms if the synsets for their component words are identical
    """
        
    @Cached(lambda *x, **y: OntologyBuilderFramework.SYNONYMS)
    def _get_synonyms(self, **state):
        """
        constructs a dictionary of synonyms to terms, where a synonym
        is defined as a list of wordnet synsets of the words in the term. 
        
        As an ad-hoc decision, it strips out some determiners, 
        and flips everything around instances of the word "of"
        """
        terms = state[OntologyBuilderFramework.TERMS]
        synonyms = {}
        for term in terms:
            subterms = map(lambda x: POSSESIVE.sub("", x),
                           filter(lambda x: x not in DETERMINERS, 
                                  term.split(' ')))
            synset_list = tuple(frozenset(synset.name for synset in wn.synsets(subterm, pos=wn.NOUN+wn.ADJ)) for subterm in subterms)
            
            if 'of' in subterms:
                indices = self.__get_indices('of', subterms)
                phrases = self.__phrasify(subterms, indices)
                synset_list = tuple(chain(*(synset_list[a[0]:a[1]] for a in reversed(phrases))))
            
            if EMPTY_SYNSET in synset_list:
                continue # just ignore anything w/out a synset
            
            if synset_list in synonyms:
                synonyms[synset_list].append(term)
            else:
                synonyms[synset_list] = [term]
                
        return synonyms

    def __get_indices(self, x, ys):
        """return indicies in ys where x occurs"""
        return filter(lambda i: ys[i] == x, xrange(len(ys)))
                
    def __phrasify(self, subterms, indices):
        """
        assuming that "indices" is a list function words, returns a list of 
        subphrases that contain actual lexical items
        """
        phrases = []
        start = 0
        for index in indices:
            stop = index
            if start < stop:
                phrases.append((start, stop))
            start = stop + 1
        phrases.append((start, len(subterms)))
        return phrases
            
        