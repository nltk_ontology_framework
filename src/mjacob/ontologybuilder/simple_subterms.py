# This Python file uses the following encoding: utf-8
'''
Created on May 7, 2011

@author: mjacob
'''

class SimpleSubterms(object):
    """
    returns a collection of subterm strings from a collection of tokens
    this version just assumes all possible substrings are possible, up to
    an optionally specified "minimal length"
    """
    def __init__(self, minlength=1):
        self.__minlength = minlength
    
    def __call__(self, term):
        for i in xrange(len(term)):
            for j in xrange(i+self.__minlength, len(term)+1):
                subterm = " ".join(term[i:j])
                yield subterm.lower()