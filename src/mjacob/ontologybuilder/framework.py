# This Python file uses the following encoding: utf-8
'''
Created on Apr 29, 2011

@author: mjacob
'''
import os
class OntologyBuilderFramework(object):
    """A framework for building domain ontologies, assuming that the framework for doing such is:
    C{TERMS}: identify terms (from a corpus or such)
    C{SYNONYMS}: identify synonyms amongst the terms
    C{CONCEPTS}: identify concepts
    C{CONCEPT_HIERARCHIES}: identify heirarchical relationships amongst concepts
    C{RELATIONS}: identify other relationships amongst concepts
    """
    
    TERMS="terms"
    SYNONYMS="synonyms"
    CONCEPTS="concepts"
    CONCEPT_HIERARCHIES="concept_hierarchies"
    RELATIONS="relations"
    
    def __init__(self, name, initial_state={}, only_do=None):
        """
        @type initial_state: C{dict}
        @param initial_state: additional state available when processing with a framework instance
        @type name: C{str}
        @param name: the name of this framework instance (used debuggin)
        @type cachedir: C{str}
        @param cachedir: the base directory in which to cache the ontology generation process
        """
        self.__state = initial_state
        self.__name = name
        self.__only_do = only_do
        
    def state(self):
        """
        get the current state.
        
        @rtype: C{dict}
        """
        return self.__state
    
    def name(self):
        """
        get the name of the framework instance.
        
        @rtype: C{str}
        """
        return self.__name
    
    def _do_step(self, step):
        """
        returns C{True} if the framework instance is designed to perform step C{step}
        
        @rtype: C{bool}
        """
        if self.__only_do and step not in self.__only_do:
            return False
        return True
    
    def __get_initial_state(self, additional_state):
        """
        construct a state collection given the defaults for object,
        and anything additional supplied for the specific run.
        """
        state = self.state().copy()
        state.update(additional_state)
        return state
    
    def process(self, **additional_state):
        """
        iterate through predefined to construct an ontology.
        
        @return the resulting state 
        """
        
        state = self.__get_initial_state(additional_state)
        
        for step in (OntologyBuilderFramework.TERMS,
                     OntologyBuilderFramework.SYNONYMS,
                     OntologyBuilderFramework.CONCEPTS,
                     OntologyBuilderFramework.CONCEPT_HIERARCHIES,
                     OntologyBuilderFramework.RELATIONS):
                         
            if self._do_step(step):
                result = self.__getattribute__('_get_%s' % step)(**state)
                
                if not result:
                    raise Exception("no result (%s) at step %s" % (result, step))
                
                state[step] = result 
                print "found %s %s" % (len(result), step)
        
        return state

class OntologyBuilderTerms(object):
    """interface for building terms for an ontology"""
    def _get_terms(self, **state):
        pass

class OntologyBuilderSynonyms(object):
    """interface for building synonyms (usually of terms) for an ontology"""
    def _get_synonyms(self, **state):
        pass

class OntologyBuilderConcepts(object):
    """interface for constructing concepts for an ontology"""
    def _get_concepts(self, **state):
        pass

class OntologyBuilderConceptHierarchies(object):
    """interafce for constructing hierarchies of concepts for an ontology"""
    def _get_concept_hierarchies(self, **state):
        pass

class OntologyBuilderRelations(object):
    """interface for building relations between concepts in an ontology"""
    def _get_relations(self, **state):
        pass
        