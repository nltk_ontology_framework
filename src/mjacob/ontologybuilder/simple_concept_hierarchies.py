# This Python file uses the following encoding: utf-8
'''
Created on May 5, 2011

@author: mjacob
'''
from framework import OntologyBuilderFramework, OntologyBuilderConceptHierarchies
from nltk.tree import Tree
from itertools import chain, combinations
from nltk.corpus import wordnet
from util.cached import Cached
from util.memoized import Memoized
wn=wordnet

class SimpleConceptHierarchies(OntologyBuilderConceptHierarchies):
    """
    constructs a hierarchical model of concepts representing terms based upon
    subterm inclusion and heirarchical relations amongst the least significant
    subterms. 
    
    the hierarchy is built in a substrate of a minimal subset of terms from
    wordnet, based around the "entity" synset.
    """
    
    def __init__(self, root_synset='entity.n.01', acceptable_pos=[wn.NOUN]):
        """initialze a couple of constants"""
        self.__root_synset = root_synset
        self.__acceptable_pos = acceptable_pos
    
    @Cached(lambda *x, **y: OntologyBuilderFramework.CONCEPT_HIERARCHIES)
    def _get_concept_hierarchies(self, **state):
        """
        given concepts, which are sequences of singular synsets, construct a tree
        of concepts around the "entity.n.01" synset in worndnet. 
        
        a concept A will be placed higher than a concept B in the Tree if
            1.  len(A) == len(B) and
                A[1:] == B[1:] and
                A[0] is a hypernym of B[0] in wordnet
            OR
            2.  len(A) < len(B) and
                B = [b0,..bn,a0,..am] where A = [a0,...,am]
        
        intermediary concepts in wordnet will be inserted to differentiate between final terms
        """
        concepts = state[OntologyBuilderFramework.CONCEPTS]
        
        groups_by_head = {}
        for concept in concepts:
            head = concept[-1]
            head_synset = wn.synset(head)
            if self.__is_a_legitimate_head_synset(head_synset):
                if head in groups_by_head:
                    groups_by_head[head].add(concept)
                else:
                    groups_by_head[head] = set((concept,))
                
        group_trees = []
        for head in groups_by_head:
            group_trees.append(self.__merge_group(head, groups_by_head[head]))
        
        merged = self.__merge_groups(group_trees)
        
        self.__sort_tree(merged)
        
        self.__attach_terms_by_concept(merged, concepts)
        
        return merged
    
    @Memoized
    def __is_a_legitimate_head_synset(self, head_synset):
        """
        returns true if the head synset is a noun, and if "entity" is the head of one of its chains 
        """
        return head_synset.pos in self.__acceptable_pos and wn.synset(self.__root_synset) in set(chain(*head_synset.hypernym_paths()))
    
    def __sort_tree(self, tree):
        """sort a subtree so that its nodes are in lexicographic order.
        note that this does not seem to work."""
        for subtree in tree:
            subtree.sort()
    
    def __attach_terms_by_concept(self, tree, concepts):
        """
        attach the actual terms discovered in the corpus to the tree
        """
        for subtree in tree.subtrees():
            if subtree.node in concepts:
                for term in concepts[subtree.node]:
                    if term not in subtree:
                        subtree.append(term)
    
    def __is_a_hypernym(self, a, b):
        """
        returns true if the concept @param a is a hypernym of the concept @param b
        """
        return self.__endswith(a, b) or (len(a) == len(b) and
                                         a[1:] == b[1:] and
                                         self.__is_synset_a_hypernym(a[0], b[0]))
        
    def __endswith(self, a, b):
        """returns true if the list b ends with the list a"""
        if len(a) >= len(b):
            return False
        
        for i in xrange(len(a)):
            if a[i] != b[len(b) - len(a) + i]:
                return False
        return True
        
    @Memoized
    def __is_synset_a_hypernym(self, a, b):
        """ returns true if the synset @param a is a hypernym of the synset @param b """
        return a != b and wn.synset(a) in chain(*wn.synset(b).hypernym_paths())
    
    def __merge_group(self, head, group):
        """merges a group of synsets with the same head"""
        tree = Tree((head,), [])
        for element in group:
            el_tree = Tree(element, [])
            self.__merge(el_tree, tree)
        
        return tree
    
    def __merge(self, element, tree):
        """merge a element of a tree into a larger tree"""
        if element in tree:
            return
        
        found_subtree = False
        for leaf in tree:
            if self.__is_a_hypernym(leaf.node, element.node):
                self.__merge(element, leaf)
                found_subtree = True
        
        was_above = False
        for leaf in tree:
            if self.__is_a_hypernym(element.node, leaf.node):
                if not leaf in element:
                    element.append(leaf)
                was_above = True
        
        if was_above:
            for leaf in element:
                if leaf in tree:
                    del tree[tree.index(leaf)]
            if not element in tree:
                tree.append(element)
        
        elif not found_subtree:
            if not element in tree:
                tree.append(element)
                            
    def __merge_groups(self, groups):
        """merge group of subtrees into a single tree, rooted at the root synset entity.n.01"""
        root_tree = None
        for group_tree in groups:
            if group_tree.node[0] == self.__root_synset:
                root_tree = group_tree
                groups = filter(lambda x: x.node[0] != self.__root_synset, groups)
                break

        if not root_tree:
            root_tree = Tree((self.__root_synset,), [])
        
        found = set((self.__root_synset,))
        for a, b in combinations(groups, 2):
            common = wn.synset(a.node[-1]).common_hypernyms(wn.synset(b.node[-1]))
            if common:
                common_node = common[0].name
                if not common_node in found:
                    self.__merge(Tree((common_node,), []), root_tree)
                    found.add(common_node)
            
        for group in groups:
            self.__merge(group, root_tree)
        
        return root_tree
    
