# This Python file uses the following encoding: utf-8
'''
Created on May 7, 2011

@author: mjacob

Implementation of term extraction filter from 
  R. Navigli and P. Velardi, “Semantic Interpretation of Terminological Strings,” Proc. 6th Int’l Conf. 
  Terminology and Knowledge Eng. (TKE 2002), INIST-CNRS, Vandoeuvre-lès-Nancy, France, 2002, pp. 95–100.

'''
from math import sqrt

class NavigliTermFilter(object):
    def __init__(self, alpha, threshold, debug=False):
        """
        @type alpha: C{float}
        @type threshold: C{float}
        """
        self.__alpha = alpha
        self.__threshold = threshold
        self.__debug = debug
    
    def __stats(self, list, name):
        """ print statistics, for use when experimenting w/ values """
        total = sum(list)
        average = total / len(list)
        variance = 0.0
        for item in list: variance += (average - item) ** 2
        std_dev = sqrt(variance / len(list))
        print "average %s: %s" % (name, average)
        print "std dev %s: %s" % (name, std_dev)
        print "max %s: %s" % (name, max(list))
        print "min %s: %s" % (name, min(list))
    
    def __call__(self, statistics, relevences, entropy):
        """
        invoke the filter on the specified statistics
        """
        terms = {}
        
        if self.__debug:
            self.__stats(relevences.values(), "relevence")
            self.__stats(entropy.values(), "entopy")
        
        scores = []
        for term in statistics:
            score = (self.__alpha * relevences[term]) + ((1-self.__alpha) * entropy[term])
            scores.append(score)
            if score > self.__threshold:
                terms[term] = score
        
        if self.__debug:
            self.__stats(scores, "score")
            print "terms before filtering: %s" % (len(statistics))
            print "terms after filtering: %s" % len(terms)
        
        return terms