# This Python file uses the following encoding: utf-8
'''
Created on May 7, 2011

@author: mjacob
'''

class SimpleChunkFilter(object):
    """
    a simple chunk filter which limits chunks to a particular part of speech, 
    and optionally a minimum length
    """

    def __init__(self, pos, minlength=1):
        self.__pos = pos
        self.__minlength = minlength
    
    def __call__(self, chunked):
        return (x.leaves() 
                for x in chunked.subtrees(lambda x: x.node == self.__pos) 
                if len(x.leaves()) > self.__minlength)
